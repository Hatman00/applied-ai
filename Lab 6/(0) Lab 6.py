#Import the tensorflow.keras.datasets.mnist module so we can load the dataset
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dense, Flatten, MaxPooling2D
from tensorflow.keras.utils import plot_model
from tensorflow.keras.models import load_model
from IPython.display import Image, display
import os, subprocess
def newsec(text):
    print("-------------------------------------------------\n" + str(text))
(X_train, y_train), (X_test, y_test) = mnist.load_data()
print("X_train.shape:\n" + str(X_train.shape) + "\ny_train.shape:\n" + str(y_train.shape) + "\nX_test.shape:\n" + str(X_test.shape) + "\ny_test.shape:\n" + str(y_test.shape))
sns.set(font_scale=2)
index = np.random.choice(np.arange(len(X_train)), 24, replace=False)
figure, axes = plt.subplots(nrows=4, ncols=6, figsize=(16, 9))
#Run this snippet multiple times to see additional randomly selected digits.
for item in zip(axes.ravel(), X_train[index], y_train[index]):
    axes, image, target = item
    axes.imshow(image, cmap=plt.cm.gray_r)
    axes.set_xticks([]) #Remove x-axis tick marks
    axes.set_yticks([]) #Ditto but for the y-axis
    axes.set_title(target)
    #axes.figure.show() Commented out so I don't see it every time.
plt.tight_layout()
#CNN require images to be in the shape (width, height, channels)
#Reshape the X_train dataset
#Why channels is 1 in this case
X_train = X_train.reshape((60000, 28, 28, 1)) #60000 samples, each sample is 28x28 and uses 1 channel. (Greyscale)
print("-------------------------------------------------\n" + str(X_train.shape))
#Now do the same for X_test
X_test = X_test.reshape((10000, 28, 28, 1)) #10000 samples, each sample is 28x28 and uses 1 channel. (Greyscale)
print("-------------------------------------------------\n" + str(X_test.shape))
#Do you still remember what is normalisation?
#Write it here: Normalisation is making the data be on a similar scale. It seems to be replacing/dropping null/NaN data and also ignoring outliers in the data?
#Now normalise the X_train
X_train = X_train.astype('float32') / 255
#Do the same for X_test
X_test = X_test.astype('float32') / 255
y_train = to_categorical(y_train)
newsec(y_train.shape)
print(y_train[0])
y_test = to_categorical(y_test)
newsec(y_test.shape)
print(y_test[0])
#Import sequential from keras models
cnn = Sequential()
cnn.add(Conv2D(filters=64, kernel_size=(3, 3), activation='relu', input_shape=(28, 28, 1)))
cnn.add(MaxPooling2D(pool_size=(2, 2)))
cnn.add(Conv2D(filters=128, kernel_size=(3, 3), activation='relu'))
cnn.add(MaxPooling2D(pool_size=(2, 2)))
cnn.add(Flatten())
cnn.add(Dense(units=128, activation='relu'))
cnn.add(Dense(units=10, activation='softmax'))
newsec(cnn.summary())
plot_model(cnn, to_file='convnet.png', show_shapes=True, show_layer_names=True)
image = Image(filename='convnet.png')
display(image) #Note: This does not work. Image apparently only works with jupyter notebook. As it is now, this will merely print the Image object in the console.
#Also, using simply Image(filename='convnet.png') on its own does nothing. ¯\_(ツ)_/¯
cnn.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
loss, accuracy = cnn.evaluate(X_test, y_test)
newsec(loss)
print(accuracy)
predictions = cnn.predict(X_test)
newsec(y_test[0])
newsec("")
for index, probability in enumerate(predictions[0]):
          print(f'{index}: {probability:.10%}')
images = X_test.reshape((10000, 28, 28))
incorrect_predictions = []
for i, (p, e) in enumerate(zip(predictions, y_test)):
    predicted, expected = np.argmax(p), np.argmax(e)
    if predicted != expected:
        incorrect_predictions.append((i, images[i], predicted, expected))
newsec("Incorrect Predictions' length: " + str(len(incorrect_predictions)))
figure, axes = plt.subplots(nrows=4, ncols=6, figsize=(16, 12))
for axes, item in zip(axes.ravel(), incorrect_predictions):
    index, image, predicted, expected = item
    axes.imshow(image, cmap=plt.cm.gray_r)
    axes.set_xticks([]) # remove x-axis tick marks
    axes.set_yticks([]) # remove y-axis tick marks
    axes.set_title(f'index: {index}\np: {predicted}; e: {expected}')
    axes.figure.show()
plt.tight_layout()
def display_probabilities(prediction):
    for index, probability in enumerate(prediction):
        print(f'{index}: {probability:.10%}')
display_probabilities(predictions[495])
display_probabilities(predictions[583])
#Save the model in a directory
#!mkdir -p saved_model (This is part of IPython and does not work here. So instead I will use the os and subprocess libraries)
os.system("mkdir -p save_model")
cnn.save('saved_model/mnist_cnn.h5') #This gives a warning that it is considered legacy.
# my_model directory
#!ls saved_model (Ditto to the above)
print(subprocess.check_output("dir /s saved_model", shell=True)) #Using subprocess because it allows for the output of the command to be printed while os does not.
# Contains an assets folder, saved_model.pb, and variables folder.
#!ls saved_model/mnist_cnn.h5 (Ditto again)
print(subprocess.check_output("dir /s mnist_cnn.h5", shell=True)) #Will have to look into this further, but I cannot specify the EXACT directory to search for this file as escaping the quotes required does not work and it
#returns an non zero status. Works fine in my CMD Prompt though so I don't know...
cnn = load_model('saved_model/mnist_cnn.h5')
cnn.summary()
#Surprisingly the end... Alrighty then...
