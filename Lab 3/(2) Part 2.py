from sklearn.datasets import load_digits
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
digits = load_digits()
X_train, X_test, y_train, y_test = train_test_split(digits.data, digits.target, random_state=11, test_size=0.20)
svc = SVC()
svc.fit(X=X_train, y=y_train)
predicted = svc.predict(X=X_test)
expected = y_test
print(predicted[:20])
print(expected[:20])
wrong = [(p, e) for (p, e) in zip(predicted, expected) if p != e]
print(wrong)
print(f'{svc.score(X_test, y_test):.2%}')
confusion = confusion_matrix(y_true=expected, y_pred=predicted)
print(confusion)
names = [str(digit) for digit in digits.target_names]
print(classification_report(expected, predicted, target_names=names))
