import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
digits = load_digits()
for data in digits:
    print(str(data) + ":\n")
    print(digits[data])
print(digits.DESCR)
print(digits.target[0: 21])
print(digits.data.shape)
print(digits.images[13])
print(digits.data[13])
print(digits.target[13])
figure, axes = plt.subplots(nrows=4, ncols=6, figsize=(6, 4))
figure.show()
_, axes = plt.subplots(nrows=1, ncols=6, figsize=(10, 3))
for axes, image, target in zip(axes, digits.images, digits.target):
    axes.set_axis_off()
    axes.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    axes.set_title('Label: %i' % target)
    axes.figure.show()
image = plt.imshow(digits.images[22], cmap=plt.cm.gray_r)
image.figure.show()
X_train, X_test, y_train, y_test = train_test_split(digits.data, digits.target, random_state=11, test_size=0.20)
print(X_train.shape)
print(y_train.shape)
knn = KNeighborsClassifier()
knn.fit(X=X_train, y=y_train)
predicted = knn.predict(X=X_test)
print(predicted)
expected = y_test
print(expected)
print(predicted[:20])
print(expected[:20])
wrong = [(p, e) for (p, e) in zip(predicted, expected) if p != e]
print(wrong)
print(f'{knn.score(X_test, y_test):.2%}')
confusion = confusion_matrix(y_true=expected, y_pred=predicted)
print(confusion)
names = [str(digit) for digit in digits.target_names]
print(names)
print(classification_report(expected, predicted, target_names=names))
confusion_df = pd.DataFrame(confusion, index=range(10), columns=range(10))
print(confusion_df)
plt.figure()
axes = sns.heatmap(confusion_df, annot=True, cmap='nipy_spectral_r')
axes.figure.show()
kfold = KFold(n_splits=10, random_state=11, shuffle=True)
scores = cross_val_score(estimator=knn, X=digits.data, y=digits.target, cv=kfold)
print(scores)
print(f'Mean accuracy: {scores.mean():.2%}')
print(f'Accuracy standard deviation: {scores.std():.2%}')
