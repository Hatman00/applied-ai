import numpy
import matplotlib.pyplot as plt
numbers = numpy.random.randint(0, 1000, 100000)
plt.hist(numbers, 100)
plt.show()
#What is the difference between a histogram and just putting all the numbers in a bar graph? Are they the same thing? Who knows?
