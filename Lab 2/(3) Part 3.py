import numpy as np
import pandas as pd

from pandas import Series, DataFrame
# Create a DataFrame object
DF_obj= DataFrame({'column 1':[1,1,2,2,3,3,3],
                   'column 2':['a', 'a','b', 'b', 'c', 'c', 'c'],
                   'column 3':['A', 'A', 'B', 'B', 'C', 'C', 'C']})
print(DF_obj)
print(DF_obj.duplicated()) #Shows which row have duplicates by showing true or false per row. (Columns cannot be checked but can be specified)
DF_obj = DF_obj.drop_duplicates()
print(DF_obj)
# Drop duplicates in certain columns
#DF_obj = DF_obj.drop_duplicates(['column 3'])
