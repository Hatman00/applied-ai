import numpy as np
import pandas as pd
data = {'Date': ['2023-01-01', '2023-01-02', '2023-01-03', '2023-01-04', '2023-01-05'],
        'Revenue': [1000, 1200, np.nan, np.nan, 1500]}
DF_obj = pd.DataFrame(data)
print(DF_obj.to_string(index=False))
DF_obj.iloc[2:4, 1] = 800
print("\nData with NaN replaced with 800:\n", DF_obj.to_string(index=False))
