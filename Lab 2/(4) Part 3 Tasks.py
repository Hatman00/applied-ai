import numpy as np
import pandas as pd

from pandas import Series, DataFrame

DF_obj = DataFrame({'OrderID': [101, 102, 103, 104, 105],
                    'CustomerID': ['A100', 'A101', 'A100', 'A102', 'A103'],
                    'Product': ['Laptop', 'Smartphone', 'Laptop', 'Tablet', 'Smartphone']})
print(DF_obj.to_string(index=False))
print(DF_obj.duplicated(subset=['CustomerID', 'Product']))
DF_obj = DF_obj.drop_duplicates(subset=['CustomerID'])
DF_obj = DF_obj.drop_duplicates(subset=['Product'])
print(DF_obj.to_string(index=False))
