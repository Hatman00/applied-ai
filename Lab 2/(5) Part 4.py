import numpy as np
import pandas as pd

from pandas import Series, DataFrame

DF_obj = pd.DataFrame(np.arange(36).reshape(6, 6))
print(DF_obj)

print("\n---------------------------------------\n")

DF_obj_2 = pd.DataFrame(np.arange(15).reshape(5,3))
print(DF_obj_2)

print("\n---------------------------------------\n")
#Concatenate horizontally (Side by side)
DF_obj_concat = pd.concat([DF_obj, DF_obj_2], axis=1)
DF_obj_concat = DF_obj_concat.fillna(0)
DF_obj_concat = DF_obj_concat.astype(int)
print(DF_obj_concat)

print("\n---------------------------------------\n")
#Concatenate vertically (Top to bottom?)
DF_obj_concat = pd.concat([DF_obj, DF_obj_2])
DF_obj_concat = DF_obj_concat.fillna(0)
DF_obj_concat = DF_obj_concat.astype(int)
print(DF_obj_concat)

print("\n---------------------------------------\n")
#Concatenate two equal dataframes
# Sample DataFrames
DF_obj = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
DF_obj_2 = pd.DataFrame({'C': [7, 8, 9], 'D': [10, 11, 12]})
result = pd.concat([DF_obj, DF_obj_2], axis=1)
print(result)
# Run the code and check did we get a decimal point as above and why?
#We did not get a decimal point because there are no decimals in any of the
#data. Concatenation is merging two or more variables together as one!

print("\n---------------------------------------\n")
print(DF_obj, "\n-------")
DF_obj = DF_obj.drop([0, 2]) #Removes the specified rows
print(DF_obj)
print(DF_obj, "\n-------")
DF_obj = DF_obj.drop(['B'], axis=1)
print(DF_obj)

print("\n---------------------------------------\n")

series_obj = Series(np.arange(6))
series_obj.name = "added_variable"
print(series_obj)

print("\n---------------------------------------\n")
variable_added = DataFrame.join(DF_obj, series_obj)
variable_added.loc[1, 'added_variable'] = 10
print(variable_added)

print("\n---------------------------------------\n")

DF_obj = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
print(DF_obj)
DF_sorted = DF_obj.sort_values(by=['B'], ascending=[False])
print("\n-------\n", DF_sorted)
