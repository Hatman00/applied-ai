# Import important libraray
import numpy as np
import pandas as pd

# Two data types: Series and DataFrame
# Series can only contain single list with index
# Dataframe is a collection of series that can be used to analyse the data (a table with rows and columns)
from pandas import Series, DataFrame

# Without an index
serios_obj = Series(np.arange(10))
print(serios_obj)
print("---------------------------------------")
# With an index
# Practice both
series_obj = Series(np.arange(10), index=['row 1', 'row 2', 'row 3', 'row 4', 'row 5', 'row 6','row 7', 'row 8', 'row 9', 'row 10'])
print(series_obj)
print(series_obj['row 2'])
print(series_obj[[0, 5]])
print("---------------------------------------")

# Populate the list with random values
np.random.seed(25)

# Search random.seed in Python and provide an explaniation here: ...
# What does seed function do?
DF_obj = DataFrame(np.random.rand(36).reshape((6,6)),
                   index=['row 1', 'row 2', 'row 3', 'row 4','row 5','row 6'],
                   columns=['column 1','column 2','column 3','column 4','column 5','column 6']) 
print(DF_obj)
print("---------------------------------------")

# Run the above code with and without np.random.seed and reflect on the difference here:
#The seed keeps the data the same each time the program is run. Without the seed, it is random, but it still uses the system time as the seed.

#Find data at different location
print(DF_obj.loc[['row 2', 'row 5'], ['column 5', 'column 2']])
# What does loc function do?
#It retrieves the data in the specified range.

print(series_obj['row 3': 'row 7'])
print("---------------------------------------")
print(DF_obj < .2) #Check all the data in the df_obj array to see if it is greater than 0.2
#Will display True or False depending.
# Retrieves all elements in series_obj that are greater than 6.
print(series_obj[series_obj > 6])

# Retrive all elements that are smaller than 5
print("---------------------------------------")

# Assign the value 8 to specific rows using .loc
series_obj.loc[['row 1', 'row 5', 'row 8']] = 8
print(series_obj)
