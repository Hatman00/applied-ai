import numpy as np
import pandas as pd
from pandas import Series, DataFrame
table1 = pd.DataFrame({'OrderID': [101, 102, 103],
                       'CustomerID': ['A101', 'A102', 'A103'],
                       'Product': ['Laptop', 'Smartphone', 'Tablet']})
table2 = pd.DataFrame({'OrderID': [104, 105, 106],
                       'CustomerID': ['A102', 'A103', 'A101'],
                       'Product': ['Monitor', 'Keyboard', 'Mouse']})
print(table1.to_string(index=False), "\n\n", table2.to_string(index=False))
combined_sales_data = pd.concat([table2, table1], axis=0)
combined_sales_data = combined_sales_data.sort_values(by=['OrderID'], ascending=True)
print(combined_sales_data.to_string(index=False))
