import numpy as np
import pandas as pd
from pandas import Series, DataFrame

nan = np.nan

series_obj = pd.Series(['row 1', 'row 2', nan, 'row 4', 'row 5', 'row 6', nan, 'row 8'])
print(series_obj)
print(series_obj.isnull())
print("\n---------------------------------------\n")

# Produce random values and shape the result in 6x6 matrix
np.random.seed(25)
DF_obj = pd.DataFrame(np.random.rand(36).reshape(6, 6))
print(DF_obj)

print("\n---------------------------------------\n")

#Recreate DF obj
DF_obj = DataFrame(np.random.rand(36).reshape((6,6)),
                   index=['row 1', 'row 2', 'row 3', 'row 4','row 5','row 6'],
                   columns=['column 1','column 2','column 3','column 4','column 5','column 6'])
# Add missing data
DF_obj.iloc[3:5, 0] = nan
DF_obj.iloc[1:4, 5] = nan
print(DF_obj)

print("\n---------------------------------------\n")

# Fill out missing data with zeros
#filled_DF = DF_obj.fillna(0)
#print(filled_DF)
# Fill the missing values with 0.5 instead of 0
# Fil out missing data in column 0
filled_DF = DF_obj['column 6'].fillna(0, axis = 0)
print(filled_DF)

print("\n---------------------------------------\n")

filled_DF = DF_obj.fillna({'column 1': 0.1, 'column 6':1.25})
print(DF_obj, "\n", filled_DF)

print("\n---------------------------------------\n")

# Fill out missing data with forward fill method
fill_DF = DF_obj.fillna(method='ffill')
print("ffill:\n", fill_DF)
# Try replace it with backward fill (bfill) and compare the answer
fill_DF = DF_obj.fillna(method='bfill')
print("bfill:\n", fill_DF)
#ffill fills in missing values with the last none NaN number while bfill fills in missing values with the NEXT none NaN number.

print("\n---------------------------------------\n")

np.random.seed(25)
DF_obj = pd.DataFrame(np.random.rand(36).reshape(6,6))
DF_obj.loc[3:5, 0] = nan
DF_obj.loc[1:4, 5] = nan
print(DF_obj)
# Counting missing data
print("\n\n", DF_obj.isnull().sum())

print("\n---------------------------------------\n")

# Drop all the raws with missing values
np.random.seed(25)
DF_obj = pd.DataFrame(np.random.rand(36).reshape(6,6))
DF_obj.loc[3:5, 0] = nan
DF_obj.loc[1:4, 5] = nan
DF_no_NaN = DF_obj.dropna()
print(DF_no_NaN)

print("\n---------------------------------------\n")

# Drop all the column with missing values
np.random.seed(25)
DF_obj = pd.DataFrame(np.random.rand(36).reshape(6,6))
DF_obj.loc[3:5, 0] = nan
DF_obj.loc[1:4, 5] = nan
DF_no_NaN = DF_obj.dropna(axis=1)
print(DF_no_NaN)

# Find out what axis=1 means?
#It supposedly only remove missing values in the rows (0) or columns (1), but both drop all missing values in my experience and the only
#difference is that 0 only shows one row when printing, while 1 shows all rows and columns both without missing values?
