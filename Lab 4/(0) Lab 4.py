import pandas
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
df = pandas.read_csv(".\\(1) Lab 4 Dataset.csv")
print(df)
#What does the read_csv function do?: It reads the specified Comma Seperated Values file and converts the data into a pandas table.
#How was the data printed?: It was printed exactly as the same as it is in the CSV file, but with the first line of values acting as headers.
#What is the label in the data above? What are the features?:

print("------------------------------------------------")
d = {'UK': 0, 'USA': 1, 'N': 2}
df['Nationality'] = df['Nationality'].map(d)
print(df)
# What does the map function do?: It maps out each value in the column to an integer as described in the dictionary represented as "d". So every instance of the "USA" becomes 1.
#Can you do the same to convert Go to numerical?:
print("----------------")
d = {"YES": 0, "NO": 1}
df['Go'] = df['Go'].map(d)
print(df)
# Can we use label encoding instead? Try to use it for Go label and print the results?: ????

print("------------------------------------------------")
#creating labelEncoder
df2 = pandas.read_csv(".\\(1) Lab 4 Dataset.csv")
le = preprocessing.LabelEncoder()
# Converting string labels into numbers
df2['Go'] = le.fit_transform(df['Go'])
print(df2)
#Converts the "Go" column to binary.
del df2

# Features list (titles should be the same as in the dataset)
print("------------------------------------------------")
features = ['Age', 'Experience', 'Rank', 'Nationality']
# Split the features from their labels
X = df[features]
y = df['Go']
# Check how many sample do we have? Reflect on the result.
print(X.shape)
#The value of X is the df table but without the Go column and, hence, it has a different shape to the normal table with it only having 4 columns rather than 5.
print("----------------")
# Check how many labels do we have?
print(y.shape)
# The value of y is

# Check the statistical information?
print("------------------------------------------------")
print(X.describe())
print("----------------")
print(y.describe())

clf = LogisticRegression()
clf = clf.fit(X.values, y)
print("------------------------------------------------")
print(clf.predict([[40, 10, 7, 1]]))
# Reflect on the result, what does it mean?: I'm not sure what it means as it merely prints "[0]". Does this mean that it predicts... Nothing?
# Add if-else to print the result in a text-based format: I would if I knew what that meant?

print("------------------------------------------------")
print(clf.predict_proba([[40, 10, 7, 1]]))
# Reflect on the result and what do these two values represented?: I do not understand what the values of "[[0.58825683 0.41174317]]" mean. :/


# Reflect on the result and what do these two values represented.
