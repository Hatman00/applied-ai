import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
df = pd.read_csv('./titanic3.csv')
orig_df = df
print(orig_df)
pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 100)
for header in df: #This prints the rows of each header. Why? 'Cause I felt like trying it.
    print(header + ":")
    print(df[header])
    print("\n")
print(df.dtypes)
print(df.shape)
print(df.describe())
print(df.describe().iloc[:,:3])
print(df.isnull().sum())
mask = df.isnull().any(axis=1)
print(mask.head()) #Print rows of the mask
print(df[mask].body.head())
print(df[mask].age.head())
print(df[mask].embarked.head())
print(df.sex.value_counts(dropna=False)) #Shows how many different values there are for the specified header and how many times that value appears
print(df.embarked.value_counts(dropna=False))
print(df['embarked'].value_counts(dropna=False))
#Delete rows with high amount of missing values
df = df.drop(columns=["name", "ticket", "home.dest", "boat", "body", "cabin"])
print(df.describe())
print(df['age'])
df['age'] = df['age'].fillna(df['age'].median())
print(df['age'])
print(df['embarked'])
df['embarked'] = df['embarked'].fillna('S')
print(df['embarked'])
df.sex = df.sex.map({'male': 1, 'female': 0})
print(df.sex)
df.embarked = df.embarked.map({'S': 2, 'C':1, 'Q':0})
print(df.embark)
#Assign survived column (targets) to y
y = df.survived
#Delete survived column from X (samples)
X = df.drop(columns="survived")
#Now split the data
X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=11)
#Check the y_train (target)
print(y_train)
#Call the ML algorithm
clf = LogisticRegresxsion(solver='liblinear')
clf.fit(X_train, y_train)
